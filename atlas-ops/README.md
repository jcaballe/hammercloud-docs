# ATLAS ops
  * [Deploy Ganga patch](./deploy_ganga_patch.html)
  * [Check dataset cache](./check_ds_cache.html)
  * [Add/remove site to/from template](./add_site_to_template.html)
  * [Debug template](./atlas-ops/debug_template_commissioning_test_submission.md)