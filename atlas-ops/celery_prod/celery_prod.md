# HammerCloud extension - `celery_prod` mode

## Template file configuration
### Identify workload to replicate
1. Identify test for template that you would like to migrate. 
   * e.g. PFT template [813](http://hammercloud.cern.ch/hc/app/atlas/template/813/) has test [20092301](http://hammercloud.cern.ch/hc/app/atlas/test/20092301/).
1. Identify recent job (not more than 4 days old) of the test.
   * Test 20092301 contains job [3004408798](http://bigpanda.cern.ch/job?pandaid=3004408798).

### Understand whether build step is needed

Find out whether the job needs a build job that does more than just copy content of the sources tarball. 

* Job [3004408798](http://bigpanda.cern.ch/job?pandaid=3004408798) of test of template 813 does not use build lib tarball as one of the inputs. 

* Job [3005911020](http://bigpanda.cern.ch/job?pandaid=3005911020) of test of template 775 does use build lib tarball (`user.gangarbt.0924043212.393559.5134.lib.tgz`) as one of the inputs, but the build lib tarball has the very same content as the sources tarball.
   * Sources tarball is shipped with HC test, it is provided in `Usercode` of the template, e.g. `AthAnalysis2.3.40_QuickAna/tarball.tgz`.

* Job [3001882975](http://bigpanda.cern.ch/job?pandaid=3001882975) of test of template 722 does use build lib tarball (`user.gangarbt.0921111055.929647.8971.lib.tgz`), and its content differs from sources tarball. Related build job: [3001882974](http://bigpanda.cern.ch/job?pandaid=3001882974).
   * `NOTICE:` As of September 2016, we decided that will handle build-requesting test with JEDI tasks `buildSpec`. Migration of such templates is postponed, pending development JEDI client API calls for better handling of task replication.

### Create draft of the new template file
For tests that do not require build job, run `configurejob.py` to prepare draft of template definition for you. 

* Grab copy of `configurejob.py` from [https://gitlab.cern.ch/hammercloud/hammercloud-atlas-software/tree/master/celery_utils/configurejob](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-software/tree/master/celery_utils/configurejob).
* Copy and paste job's `jobParameters` from the job page in BigPanDA monitor to a text file, e.g. `/tmp/jp.813.txt`.
   * `jobParameters` for a job of template 813 are e.g.

   ```
--maxEvents=2 --skipEvents=0 --firstEvent=118001 --randomSeed=119 --DBRelease=current --geometryVersion=ATLAS-R2-2015-03-01-00_VALIDATION --conditionsTag=OFLCOND-RUN12-SDR-19 --physicsList=FTFP_BERT --preExec="EVNTtoHITS:simFlags.TRTRangeCut=30.0" --preInclude="EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py" --postInclude="PyJobTransforms/UseFrontier.py" --simulator=MC12G4 --truthStrategy=MC15aPlus --runNumber 361106 --outputHitsFile Hits.hc_20092301.HITS.pool.root.INFN-ROMA1.1474593990.a0db --inputEvgenFile EVNT.04972714._000021.pool.root.1 --overwriteQueuedata={allowfax=False}         
         ```

* Run the `configurejob.py` script to get the template file draft. E.g.
   
   ``` 
# python configurejob.py 3004408798 /tmp/jp.813.txt
   ```
   * Output is written to a file mentioned in the script output (e.g. `/tmp/draft_template_from_3004408798.tpl.2016-09-25.141359`):
      
         ```
         INFO    Running:        configurejob.py 3004408798 /tmp/jp.813.txt
         INFO    Usage:  configurejob.py PandaID /path/to/file/with/jobparameters.txt buildPandaID /path/to/file/with/buildjobparameters.txt
         WARNING Unknown PanDA ID of a build job " None ".
         INFO    Let's begin.
         INFO    Job page URL: http://bigpanda.cern.ch/job?json&pandaid=3004408798
         INFO    Fetched content of URL http://bigpanda.cern.ch/job?json&pandaid=3004408798 into file /tmp/draft_template_from_3004408798.tpl.2016-09-25.141359_INPUT
         INFO    Crunching data.
         INFO    Got result.
         INFO    Wrote result in file /tmp/draft_template_from_3004408798.tpl.2016-09-25.141359

         ```
   * If you need to get configuration with build job, run the script as 
   
   ``` 
# python configurejob.py 3004408798 /tmp/jp.813.txt ${BUILD_JOB_ID} ${PATH_TO_BUILD_JOB_JOBPARAMETERS_FILE}
   ```

### Adapt draft of the new template file
Edit the template draft. Focus mainly on parts that are marked by `### TODO FIXME ###`.

* In `[jobs]` section, choose whether you need build job or not. Set `list_jobs` accordingly, e.g. 
   
   ```
list_jobs = workjob   
   ```
   
* In `[input_data_exploration]` section, choose `data_exploration_method`: 
   * `exact`: If you wish to use inputs with a particular file of a particular dataset, use `data_exploration_method = exact`. The file has to have replica on RSEs available to the PanDA resource where the job will run. If the input data is missing or dataset replica is not complete, job submission will fail.
      * If using `exact` data exploration method, set `prodDBlock` in the `[workjob.jobspec]` section to the name of input dataset of the first input file. 
   * `random`: If you wish to say how many files of a input dataset pattern without hardcoding the filename or dataset name, use `data_exploration_method = random`. Then the inputDS has to have a complete replica on RSEs available to the PanDA resource where the job will run. If the input data is missing or dataset replica is not complete, job submission will fail.
      * For `random` data exploration choice, adapt `list_input_dspattern_identifiers`.
         * The `list_input_dspattern_identifiers` is a comma-delimited list of identifiers of the input dataset name patterns. 
         * Replace the proposed dataset name by pattern from the template, the result can be e.g. 
            
         ```
  INPUTDSPATTERN00.dsn = mc15_13TeV:mc15_13TeV.*.merge.AOD.*_r663*_r6264*
```
            
         * Leave out any build lib inputs for now. Comment them out by placing a `#` character at the beginning of the line.
         * In case you wish to specify multiple input DSname patterns for nfiles, e.g. pick 1 file out of multiple input DSname patterns, list the patterns separated by space, e.g. 
         
            ```
             INPUTDSPATTERN00.dsn = mc12_8TeV:mc12_8TeV.*evgen.EVNT*       valid2:valid2.*evgen.EVNT*
             INPUTDSPATTERN00.nfiles = 1

            ```
         
         
* In `[workjob.jobspec]` section: 
  * Set `NOBUILD = True`. 
  * Configure various job properties, e.g. `prodSourceLabel`, `processingType` and others. 
  * Configuring `jobParameters`: 
     * If using `random` data exploration method, make sure input file declarations use the input dataset name patterns identifiers, e.g. instead of `FILEI00 = ###fileI00###` use rather `FILEI00 = ###INPUTDSPATTERN00.file0###`.
        * In case you misconfigure job input file name masks, you will see strings such as `###fileI00###` in the BigPanDA monitor job page in job parameters. Such a job will fail.
     * Remove the build library reference from `job.jobParameters`, e.g. remove string ` -l ###FILEI01### ` from `job.jobParameters`.
     * Remove the sources tarball reference from `job.jobParameters`, e.g. remove string ` -a source.20092023.tar.gz` or ` -o source.20092023.tar.gz` from `job.jobParameters`.
     * Remove the `sourceURL` from `job.jobParameters`, e.g. remove string ` --sourceURL https://aipanda011.cern.ch:25443 ` from `job.jobParameters`.

* In `[workjob.files]` section: 
  * Set `list_files`:
     * If using `exact` data exploration method, you need to provide list of file identifier for all `input`, `pseudo_input`, `output`, and `log` files. 
     * If using `random`  data exploration method, you need to provide list of file identifier for all `pseudo_input`, `output`, and `log` files, you do not provide `input` identifiers.
  * `workjob Inputs`: 
     * For `exact` data exploration method, make sure the files are configure properly.
     * For `random` data exploration method, you do not need to care about `input` files.
  * `workjob Outputs`:
     * For `exact` data exploration method, make sure the files are configure properly.
     * For `random` data exploration method, update `dataset`, `destinationDBlock`, and `lfn` fields of the `output` and `log` files according to the proposal in the template draft. 
        * You can and should use `###JOBNAME###` variable as a part of `lfn`. 
        * You can use further variables: `###SITE###`, `###OUTPUTDATASETNAME###`, `###TESTID###`.

### Get the new template ready
1. Commit the template to the repository, deploy to submit nodes (new hostgroup `wlcghammercloud/subwcelery/atlas`, e.g. `hammercloud-ai-20`).
2. Save template in HC admin, start the test.





## Template migration
To migrate a template to HammerCloud extension, you need to create a new `.tpl` file and register new template in HC admin. You can create the new `.tpl` file following instructions in [Template file configuration](#template-file-configuration) section.

As mentioned in [Template file configuration](#template-file-configuration) section, for now we cannot migrate templates that include real build job: 
   
   * AFT [722](http://hammercloud.cern.ch/hc/app/atlas/template/722/), 
   * RCM [749](http://hammercloud.cern.ch/hc/app/atlas/template/749/).




## Job priority
By default, job priority is set to:
   
   * 10000 for production jobs, 
   * 1000 for analysis jobs, 
   * 5000 for build jobs.

If you need to run jobs with a low priority (e.g. for backfilling a cloud-based PanDA resource, or for non-invasive stress testing when there is lack of real workload), follow [`celery_prod_lowpriority` mode](celery_prod_lowpriority.html) instructions. 


## Input file randomization
If you need randomized inputs for the HC test, across sites and different input for different jobs, use ``data_exploration_method = exact`` (described in section [Adapt draft of the new template file](#adapt-draft-of-the-new-template-file)). 

This data exploration method takes input dataset names from available datasets with complete replica at the site, and selects random one. 
For this dataset, desired number of files is picked. Files are selected randomly. 

When a job of a test is cloned, for each input dataset name pattern the dataset name is re-used, but new set of files is picked.


## Override template configuration with extraargs
This functionality is now available in the HC extension. There are 3 modes of operating job properties, ``SET`` to set value of JobSpec property, ``ADD`` to append string to a JobSpec property, and ``SUB`` to substitute value of the parameter in a JobSpec property.

### extraargs: general remarks
* Each extraargs token consists of 3 parts: 
   * Action: ``SET``, or ``ADD``, or ``SUB``.
   * Field: string in rectangular brackets, e.g. ``job.currentPriority``. The same name as in the ``.tpl`` file, following the case-sensitive naming convention of `JobSpec()` properties.
   * Value: string following right after ``=`` after the field. It is interpreted differently for different actions. E.g. ``--overwriteQueuedata={use_newmover=True}``.

### extraargs: ``SET``
In order to set different job priority, the ``444``, add the following string into ``extraargs`` field of the template in template admin:  

   ```
  SET[job.currentPriority]=444 
```
``WARNING:`` if you use HC extension mode ``celery_prod_lowpriority``, the ``SET[job.currentPriority]`` extraargs will not be taken into account! Please use HC extension mode ``celery_prod`` to override job priority from extraargs.


### extraargs: ``ADD``
In order to append string to jobParameters, e.g. to overwrite queue configuration: 

   ```
   ADD[job.jobParameters]=--overwriteQueuedata={use_newmover=True} 
   ```
``WARNING:`` if the template file (``.tpl``) already contains the ``--overwriteQueuedata`` option, do not use ``ADD`` extraarg mode, use the ``SUB`` mode instead! 

### extraargs: ``SUB``
In order to substitute string in ``jobParameters``, e.g. change overwrite of queue configuration from ``--overwriteQueuedata={use_fax=False}`` to ``--overwriteQueuedata={use_newmover=True,catchall=log_to_objectstore}``:

   ```
   SUB[job.jobParameters]=--overwriteQueuedata={use_newmover=True,catchall=log_to_objectstore} 
   ```

### extraargs for build jobs
Build jobs allow 3 modes of operating job properties, ``BUILDSET`` to set value of JobSpec property, ``BUILDADD`` to append string to a JobSpec property, and ``BUILDSUB`` to substitute value of the parameter in a JobSpec property.



## Debug job configuration outside HC
In case you wish to debug configuration of jobs for your new future test outside HammerCloud and its templates, you can. The process involves using PanDA server's test job options (jO) and adapt the `JobSpec()` and `FileSpec()` objects accordingly. 

* Overview of PanDA server's test jOs: [https://github.com/PanDAWMS/panda-server/tree/master/pandaserver/test](https://github.com/PanDAWMS/panda-server/tree/master/pandaserver/test)

* Example of jO for HC extension job development: [https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/tree/master/apps/atlas/inputfiles/templates/CeleryProd](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/tree/master/apps/atlas/inputfiles/templates/CeleryProd)

   * e.g. jO for reco(pile) job: [pile_2075.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/blob/master/apps/atlas/inputfiles/templates/CeleryProd/pile_2075.py).
   
   * e.g. jO for a nightly FT job, needs upload of sources tarball to PanDA cache server before the job submission: [nightly_test.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles/blob/master/apps/atlas/inputfiles/templates/CeleryProd/nightly_test.py).

   * Ask HC support team for access privileges if you cannot access HammerCloud's GitLab repository at CERN.  

* Environment configuration example: 

    ```
### environment setup
export PANDA_DEBUG=True
export X509_USER_PROXY=/data/jschovan/x509up
export PYTHONPATH=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/PandaClient/current/lib/python2.6/site-packages
### run job submission
# python pile_2075_test.py CERN-EXTENSION_MCORE CERN
...
PandaID=2970900330
```

Once you figure out the proper job configuration, you can use it in definition of a new template (`.tpl` file) following [Template file configuration](#template-file-configuration) section.

`NOTICE`: please note that in order to define `job.jobParameters` in the python jO, you need to escape quote and slash characters: 
   * If the `jobParameters` field for a job on BigPanDA monitor contains sub-string `[\"HepMcParticleLink\"]`, you need to update it to `[\\\"HepMcParticleLink\\\"]` in your python jO, but keep it `[\"HepMcParticleLink\"]` in the `.tpl` template file, i.e. replace `"` by `\"`, and replace `\` by `\\` for `jobParameters` in python jO.  

