# HammerCloud extension - `celery_prod` mode
  * [Normal priority jobs](./celery_prod.html) (`celery_prod` mode)
  * [Low priority jobs](./celery_prod_lowpriority.html) (`celery_prod_lowpriority` mode)
  * [Template file configuration](./celery_prod.html#template-file-configuration)
  * [Template migration](./celery_prod.html#template-migration)
  * [Job priority](./celery_prod.html#job-priority)
  * [Override template configuration with extraargs](./celery_prod.html#override-template-configuration-with-extraargs)
