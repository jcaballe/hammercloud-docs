# Check dataset cache
The dataset cache is used for exploration of input datasets for HC test at sites. 

The dataset cache is based on Rucio dump [https://rucio-hadoop.cern.ch/consistency_datasets?rse=*](https://rucio-hadoop.cern.ch/consistency_datasets?rse=*), it does not involve DDM clients. 

The dataset cache is produced on `hammercloud-ai-55` and distributed to all submit nodes. 

When a site is not receiving HC test jobs due to missing dataset of a certain pattern, you can check whether the dataset had a complete replica at a particular RSE before the dump was created. 


1. Locate the `stdouterr.txt` log of the test, e.g. for test [http://hammercloud.cern.ch/hc/app/atlas/test/20086026/](http://hammercloud.cern.ch/hc/app/atlas/test/20086026/) the log directory is [https://hc-ai-core.cern.ch/testdirs/atlas/test_20086026/](https://hc-ai-core.cern.ch/testdirs/atlas/test_20086026/) and the `stdouterr.txt` location is [https://hc-ai-core.cern.ch/testdirs/atlas/test_20086026/stdouterr.txt](https://hc-ai-core.cern.ch/testdirs/atlas/test_20086026/stdouterr.txt).
2. Look for entries related to the affected site in the `stdouterr.txt` log. E.g. for `ANALY_TRIUMF_PPS`: 

  ```
 INFO 2016-06-29 11:33:25,173 test_generate 16394 140205561763584 Site ANALY_TRIUMF_PPS locations [u'TRIUMF-LCG2-MWTEST_SCRATCHDISK', 'TRIUMF-LCG2-MWTEST_DATADISK', 'TRIUMF-LCG2_LOCALGROUPDISK', 'TRIUMF-LCG2_DATADISK']
 ...
 INFO 2016-06-29 11:36:41,455 test_generate 16394 140205561763584 *** Generating 1 jobs for site ANALY_TRIUMF_PPS (ddm location TRIUMF-LCG2-MWTEST_SCRATCHDISK)
 ERROR 2016-06-29 11:36:41,456 test_generate 16394 140205561763584 Assertion error: [assert mode.startswith("prod")] while mode=default
 INFO 2016-06-29 11:36:41,456 test_generate 16394 140205561763584 00 datasets at location TRIUMF-LCG2-MWTEST_SCRATCHDISK (not using dspatterns) 
 INFO 2016-06-29 11:36:41,456 test_generate 16394 140205561763584 skipping TRIUMF-LCG2-MWTEST_SCRATCHDISK

 INFO 2016-06-29 11:36:41,456 test_generate 16394 140205561763584 *** Generating 1 jobs for site ANALY_TRIUMF_PPS (ddm location TRIUMF-LCG2-MWTEST_DATADISK)
 ERROR 2016-06-29 11:36:41,456 test_generate 16394 140205561763584 Assertion error: [assert mode.startswith("prod")] while mode=default
 INFO 2016-06-29 11:36:41,457 test_generate 16394 140205561763584 01 datasets at location TRIUMF-LCG2-MWTEST_DATADISK (not using dspatterns) 
 INFO 2016-06-29 11:36:41,457 test_generate 16394 140205561763584 TRIUMF-LCG2-MWTEST_DATADISK is enabled
 INFO 2016-06-29 11:36:41,457 test_generate 16394 140205561763584 copying /tmp/sources.20086026.derivation.tgz to /data/hc/apps/atlas/inputfiles/templates/Derivation-20.7.6.4/derivation.tgz
 INFO 2016-06-29 11:36:41,468 test_generate 16394 140205561763584 copying /tmp/Derivation-20.7.6.4/Reco_tf.py to /data/hc/apps/atlas/inputfiles/templates/Derivation-20.7.6.4/Reco_tf.py
 INFO 2016-06-29 11:36:41,469 test_generate 16394 140205561763584 Generated: ANALY_TRIUMF_PPS: TRIUMF-LCG2-MWTEST_DATADISK, 1, ['data15_13TeV:data15_13TeV.00276336.physics_Main.merge.AOD.r7562_p2521_tid07709524_00'] 
 INFO 2016-06-29 11:36:41,469 test_generate 16394 140205561763584 01 datasets generated at ANALY_TRIUMF_PPS: TRIUMF-LCG2-MWTEST_DATADISK
 INFO 2016-06-29 11:36:41,469 test_generate 16394 140205561763584 ANALY_TRIUMF_PPS has no more jobs needed. Skipping TRIUMF-LCG2_LOCALGROUPDISK
 INFO 2016-06-29 11:36:41,470 test_generate 16394 140205561763584 ANALY_TRIUMF_PPS has no more jobs needed. Skipping TRIUMF-LCG2_DATADISK

  ```
  * The log lists two attempts to generate a job for `ANALY_TRIUMF_PPS` (`*** Generating 1 jobs for site ANALY_TRIUMF_PPS`, for RSEs `TRIUMF-LCG2-MWTEST_SCRATCHDISK` and `TRIUMF-LCG2-MWTEST_DATADISK`).
  * Only the attempt at `TRIUMF-LCG2-MWTEST_DATADISK` was successful, because this RSE contained the dataset of the desired pattern: `01 datasets generated at ANALY_TRIUMF_PPS`.

3. You can find information about dataset replicas in Rucio UI. 
  * E.g. job [2904947986](http://bigpanda.cern.ch/job?pandaid=2904947986) uses input dataset `data15_13TeV:data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00`. 
  * Information about input dataset replicas is in Rucio UI: [https://rucio-ui.cern.ch/did?scope=data15_13TeV&name=data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00](https://rucio-ui.cern.ch/did?scope=data15_13TeV&name=data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00). 
    * There are 3 replicas of this dataset:
      ```
IN2P3-LPSC_DATADISK AVAILABLE 672 2.69 TB Thu, 11 Feb 2016 13:01:05 UTC Mon, 29 Feb 2016 14:31:52 UTC
TRIUMF-LCG2_DATADISK AVAILABLE 672 2.69 TB Thu, 11 Feb 2016 13:01:18 UTC Thu, 30 Jun 2016 00:39:33 UTC
TRIUMF-LCG2_DATATAPE AVAILABLE 672 2.69 TB Sun, 20 Mar 2016 19:17:09 UTC Fri, 08 Apr 2016 13:05:18 UTC
      ```
    * Neither of the RSEs listed is registered (in HC database, `Site.ddm`) for `ANALY_TRIUMF_PPS`: only `TRIUMF-LCG2-MWTEST_SCRATCHDISK`, `TRIUMF-LCG2-MWTEST_DATADISK`, `TRIUMF-LCG2_LOCALGROUPDISK`, `TRIUMF-LCG2_DATADISK` are registered.
      * You can see which RSEs are registered on HC page: from [http://hammercloud.cern.ch/hc/app/atlas/sites/](http://hammercloud.cern.ch/hc/app/atlas/sites/) go to the site page, e.g. [http://hammercloud.cern.ch/hc/app/atlas/site/733/](http://hammercloud.cern.ch/hc/app/atlas/site/733/) for `ANALY_TRIUMF_PPS` and check the **DDM** field. 
    * Therefore this HC job failed due to unavailable inputs. 

4. You can check if the dataset is known in the Rucio dump, used for the cache. 
  * Explore the dumps on `hammercloud-ai-55`:
    
    ```
 # ssh aiadm
 # ssh hammercloud-ai-55
 # cd /tmp
 # ls -lh /tmp/dataset_full* 
 -rw-r--r-- 1 root root 907M Jun 30 09:00 /tmp/datasets_full
 -rw-r--r-- 1 root root 900M Jun 29 09:00 /tmp/datasets_full.old
     
    ```    
  * Check dataset presence in the Rucio dumps:
 
```
 # grep data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00 /tmp/datasets_full*

/tmp/datasets_full:IN2P3-LPSC_DATADISK	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00
/tmp/datasets_full:TRIUMF-LCG2_DATADISK	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00
/tmp/datasets_full:TRIUMF-LCG2_DATATAPE	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00

/tmp/datasets_full.old:IN2P3-LPSC_DATADISK	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00
/tmp/datasets_full.old:TRIUMF-LCG2_DATADISK	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00
/tmp/datasets_full.old:TRIUMF-LCG2_DATATAPE	data15_13TeV	data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521_tid07687746_00
     
  ```    

  * The dataset is not present at registered RSEs. The Rucio dump is reflected in dataset cache distributed to the submit nodes. 


## Final remarks
  * If there is a complete replica of a dataset at a RSE listed in the Rucio UI, but the Rucio dump on `hammercloud-ai-55` does not contain it, we have to wait for the new Rucio dump to be generated (and consumed by HammerCloud) in order to be able to run HC jobs with this dataset as input. Rucio dump is created each day between 6am and 9am. The HC cronjob that consumes Rucio dump runs every day, around 9am. 
  * If the dataset is present in Rucio dump [https://rucio-hadoop.cern.ch/consistency_datasets?rse=*](https://rucio-hadoop.cern.ch/consistency_datasets?rse=*), but it is not in `/tmp/datasets_full` file on `hammercloud-ai-55`, the dataset cache creating cronjob has to be run. 
  